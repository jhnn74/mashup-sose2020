const iconElement = document.querySelector(".weather-icon");
const temperatureElement = document.querySelector(".temperature-value");
const inputElement = document.getElementById("cityInput");

const key = "e2b0e67737103af1e9979b512c3ed515";

var mymap = L.map('map').setView([51.505, -0.09], 5);

var marker;

initializeMap();

function initializeMap() {
    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 18,
        id: 'mapbox/streets-v11',
        tileSize: 512,
        zoomOffset: -1,
        accessToken: 'pk.eyJ1IjoicGFscHJvdGVpbiIsImEiOiJja2Vicm9na3AwYjFhMnNydnhoOTRhazdsIn0.qt3wmGLMAgq8W84-nJZL4g'
        }).addTo(mymap);
}

const weather = {};

weather.temperature = {
    unit : "celsius"
}

function getWeather(inputValue) {
    var inputValue = inputElement.value;

    let api = `https://api.openweathermap.org/data/2.5/weather?q=${inputValue}&appid=${key}`;

    fetch(api)
        .then(function(response) {
            let data = response.json();
            return data;
        })
        .then(function(data){
            weather.temperature.value = Math.floor(data.main.temp - 273);
            weather.description = data.weather[0].description;
            weather.iconId = data.weather[0].icon;
            weather.city = data.name;
            weather.country = data.sys.country;
            mymap.setView([data.coord.lat, data.coord.lon], 8);
            marker = L.marker([data.coord.lat, data.coord.lon]).addTo(mymap);
            getCoronaSummary();
        })
        .then(function(){
            displayWeather();
        });
}

function displayWeather(){
    iconElement.innerHTML = `<img class="animated bounceIn" src="icons/${weather.iconId}.png" alt="error"/>`;
    temperatureElement.innerHTML = `<h3 class="animated fadeIn">Current Weather Information: ${weather.temperature.value}°C</h3></p>`;
}

function getCoronaSummary() {
    let api = `https://api.covid19api.com/total/country/${weather.country}`;
    mymap.closePopup();

    fetch(api)
        .then(function (response) {
            let obj = response.json();
            return obj;
        })
        .then(function(obj) {
            var lastElement = obj[obj.length -1];
            var popup = L.popup()
                .setContent(`<h3>You're here: ${weather.city}, ${weather.country}</h3><p>State of Corona in this Country:</p> <p>Confirmed: ${lastElement.Confirmed}</p><p>Deaths: ${lastElement.Deaths}</p>
                <p>Recovered: ${lastElement.Recovered}</p><p>Active: ${lastElement.Active}`);
            marker.bindPopup(popup);
        })
}