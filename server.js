const express = require('express');
const app = express();

app.use(express.static('public'));
const port = process.env.PORT || 1337;

app.listen(port, function() {
    console.log("Listening at port 1337!");
});

app.get('/', function(req, res) {
    res.sendFile('index.html');
});