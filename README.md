# MashUp-SoSe2020

## About

VacationCheck is an mashup-app used to give information about the current weather and corona situation in a country/city.
The REST APIs it's using are: Covid19API, leaflet and openweathermap.

## Installation

 - Clone the repository
 - Run "npm install" in your console
 - Run "npm start" to start the app

The app is running on your "localhost:1337" now.

## How to use

Just type in the name of the city you're looking for - VacationCheck will show you the current weather and create a marker on the interactive map, which displays the current corona situation in this country. The markers won't get deleted so you can compare the corona situation in different countries.
